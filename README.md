# My-Django-Post-s
Contains link to my django posts

Post 1: https://padhle.com/django-introduction/

Post 2: https://padhle.com/writing-your-first-blog-app-in-django/ 

Post 3: https://padhle.com/url-in-django/

Post 4: https://padhle.com/views-in-django/

Post 5: https://padhle.com/writing-your-first-blog-app-in-django-part-2/ 

Post 6: https://padhle.com/templates-in-django/

Post 7: https://padhle.com/models-in-django-free-django-tutorials/

Post 8: https://padhle.com/writing-your-first-blog-app-in-django-part-3/

Post 9: https://padhle.com/django-orm-and-querysets-free-django-tutorials/

Post 10: https://padhle.com/the-django-admin-site-free-django-tutorials/

Post 11: https://padhle.com/writing-your-first-blog-app-in-django-part-4/ 

Post 12: https://padhle.com/writing-your-first-blog-app-in-django-part5/ 

Post 13: https://padhle.com/writing-your-first-blog-app-in-django-part6/ 

Post 14: https://padhle.com/writing-your-first-blog-app-in-django-part7/ 

Post 15: https://padhle.com/writing-your-first-blog-app-in-django-part8/ 

Post 16: https://padhle.com/writing-your-first-blog-app-in-django-part9/ 

Post 17: https://padhle.com/writing-your-first-blog-app-in-django-part10/ 

Post 18: https://padhle.com/writing-your-first-blog-app-in-django-part11/ 

Post 19: https://padhle.com/writing-your-first-blog-app-in-django-part12/ 

Post 20: https://padhle.com/writing-your-first-blog-app-in-django-part13/ 

Post 21: https://padhle.com/writing-your-first-blog-app-in-django-part14/ 

Post 22: https://padhle.com/writing-your-first-blog-app-in-django-part15/ 

Post 23: https://padhle.com/writing-your-first-blog-app-in-django-part16/ 

Post 24: https://padhle.com/writing-your-first-blog-app-in-django-part17/ 

Post 25: https://padhle.com/writing-your-first-blog-app-in-django-part18/ 

Post 26: https://padhle.com/writing-your-first-blog-app-in-django-part19/ 

Post 27: https://padhle.com/writing-your-first-blog-app-in-django-part20/ 

Post 28: https://padhle.com/writing-your-first-blog-app-in-django-part21/ 

Post 29: https://padhle.com/writing-your-first-blog-app-in-django-part22/ 

Post 30: https://padhle.com/writing-your-first-blog-app-in-django-part23/

Post 31: https://padhle.com/more-about-models-in-django/ 

Post 32: https://padhle.com/relationships-in-models-in-django/ 

Post 33: https://padhle.com/build-a-todo-list-app-in-django-part-1/ 

Post 34: https://padhle.com/how-to-work-with-forms-in-django/ 

Post 35: https://padhle.com/build-a-todo-list-app-in-django-part-2/ 

Post 36: https://padhle.com/build-a-todo-list-app-in-django-part-3/ 

Post 37: https://padhle.com/build-a-todo-list-app-in-django-part-4/ 

Post 38: https://padhle.com/api-in-forms-free-django-tutorials/ 

Post 39: https://padhle.com/more-on-forms-in-django-and-api-in-forms/ 

Post 40: https://padhle.com/url-configurations-in-the-django-view-layer/

Post 41: https://padhle.com/sending-emails-in-django-free-django-tutorials/ 

Post 42: https://padhle.com/how-to-use-django-with-apache/ 

Post 43: https://padhle.com/publishing-a-django-website-to-a-heroku-web-server/ 

Post 44: https://padhle.com/file-uploading-in-django/ 

Post 45: https://padhle.com/sessions-in-django/ 

Post 46: https://padhle.com/djangos-comments-framework/ 

Post 47: https://padhle.com/django-ajax-rss/

Post 48: https://padhle.com/creating-a-weather-app-in-django-part1/ 

Post 49: https://padhle.com/creating-a-weather-app-in-django-part2/ 

Post 50: https://padhle.com/creating-a-weather-app-in-django-part3/

Post 51: https://padhle.com/creating-a-weather-app-in-django-part4/ 

